

import React, { Component } from 'react'

export default class LocationList extends Component {
  render() {
    return (
      <div>
        {
          this.props.locations.map(singleLocation =>
            <div key={singleLocation.id}>
              <h3>{singleLocation.name}</h3>
              <p>{singleLocation.address}</p>
            </div>)
        }
      </div>

    );
  }
}