
import React, { Component } from "react"
import ApplicationViews from "./ApplicationViews.js"
import Navbar from "./nav/NavBar"
import "./Kennel.css"


class Kennel extends Component {



  render() {
    return (
      <article className="kennel">
        <Navbar />
        <ApplicationViews />

        {/* <LocationList locations={this.state.locations} />
        <EmployeeList employees={this.state.employees} />
        <AnimalList animals={this.state.animals} /> */}
      </article>
    )
  }
}

export default Kennel