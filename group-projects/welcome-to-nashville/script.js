// ------------------------------ parks --------------------------------------------//


// get the input from the search bar and store it in a variable
// for example, if they type in "Dog Park" you might end up with this variable:
let parkInput = "Dog Park";

// the api is expecting something like dog_park, so we gotta get our input into that format
// convert it all to lowercase and split it on the space.
parkInput = parkInput.toLowerCase().split(" ");

// build a string with an underscore instead of a space
parkInput = `${parkInput[0]}_${parkInput[1]}`


fetch(`https://data.nashville.gov/resource/xbru-cfzi.json?${parkInput}=Yes`, {
  "$$app_token": "Ttu4WHzeYGZ9vPMD2TKMEs8lr"
})
  .then(parks => parks.json())
  .then(parkData => {
    // check here to see if any data comes back. if not, print an error message
    console.log(parkData)
  });

//------------------------------ zomato ------------------------------------------- //


const cuisine = "italian";

fetch(`https://developers.zomato.com/api/v2.1/search?entity_id=1138&entity_type=city&q=${cuisine}`, {
  headers: {
    "Accept": "application/json",
    "user-key": "946f4090d2ad2d83ec92dc2906830653"
  }
})
  .then(r => r.json())
  .then(restaurants => {
    console.log(restaurants)
  })


// -------------------------- eventbrite ---------------------------------------------//



const searchField = "coding";

fetch(`https://www.eventbriteapi.com/v3/events/search/?q=nashville_${searchField}&token=DJDVYEQRBZRWWJ6PKFLG`, {
  headers: {
    "Authorization": "Bearer DJDVYEQRBZRWWJ6PKFLG",
    "Accept": "application/json"
  }
})
  .then(r => r.json())
  .then(meetups => {
    console.log(meetups);
  });


// --------------------------- ticketmaster --------------------------------- //



const genre = "rock";

fetch(`https://app.ticketmaster.com/discovery/v2/events.json?classificationName=music&dmaId=312&apikey=IB2HWZ6743NL9hnHSQSvCgywone1t0Kk&classificationName=${genre}`)
  .then(r => r.json())
  .then(concerts => {
    console.log("concerts", concerts);
  })
