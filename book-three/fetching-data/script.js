fetch("http://localhost:8088/food")
  .then(foods => foods.json())
  .then(parsedFoods => {
    console.table(parsedFoods)
    let htmlString = "";
    parsedFoods.forEach(food => {
      // THIS IS FOR THE SECOND EXERCISE
      fetch(`https://world.openfoodfacts.org/api/v0/product/${food.barcode}.json`)
        .then(response => response.json())
        .then(productInfo => {
          // add the info to the foods
          food.ingredients = productInfo.product.ingredients;
          food.countryOfOrigin = productInfo.product.countries;
          food.calories = `${productInfo.product.nutriments.energy} ${productInfo.product.nutriments.energy_unit}`
          // do the same for fat and sugar
          htmlString += foodFactory(food);
          document.querySelector("#foodlist").innerHTML = htmlString;
        })
    })

  })


// Create a function which returns a string template.The template is the HTML representation for a food item.
const foodFactory = singleFood => {
  console.log(singleFood);
  let foodHTML = `
  <div>
      <h1>${singleFood.name}</h1>
      <p>${singleFood.type}</p>
      <p>${singleFood.ethnicity}</p>
  `
  if (singleFood.ingredients) {
    // this data structure isn't exactly like but close enough 
    foodHTML += `<p>${singleFood.ingredients.join(", ")}`
  }
  if (singleFood.countryOfOrigin) {
    foodHTML += `<p>${singleFood.countryOfOrigin}`;
  }
  if (singleFood.calories) {
    foodHTML += `<p>${singleFood.calories}`;
  }

  // you normally wouldn't need  these if statements, except I didn't bother putting barcods on all of my products

  return foodHTML;
}

// Create a function that inserts an HTML representation of a food into the DOM
// Example fetch call using functions


// HERE'S ANOTHER WAY TO DO STEP ONE USING THE BOILERPLATE CODE

// let allFoodsHTML = ""

// // Function that returns an HTML string
// const foodFactory = foodParam => `<section><h2>${foodParam.name}</h2><p>Ethnicity: ${foodParam.ethnicity}</p><p>Category: ${foodParam.category}</p></section>`

// // Function that adds a single food's html string to the global variable that's collecting ALL our food HTML strings as we loop
// const addFoodToDom = singleFoodHTMLString => allFoodsHTML += singleFoodHTMLString;

// fetch("http://localhost:8088/food")
//   .then(foods => foods.json())
//   .then(parsedFoods => {
//     console.log(parsedFoods)
//     // loop through the foods
//     parsedFoods.forEach(singleFoodObj => {
//       const foodAsHTML = foodFactory(singleFoodObj)
//       addFoodToDom(foodAsHTML)
//     })
//     document.querySelector("#container").innerHTML = allFoodsHTML;
//   })

