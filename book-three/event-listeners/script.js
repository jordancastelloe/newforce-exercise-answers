// Create an input field in your DOM. Give it an id of message.
// Create two article elements with unique id values. Use Flexbox to display them in a row, each taking 50% of the width of the browser.
// Give each article a different border color.
// Write an event listener that listens for the keyup event on the input field.
// The event handler function should update the textContent property of both sections.

const messageInput = document.querySelector("#message");
messageInput.addEventListener("keyup", () => {
  document.querySelectorAll("article").forEach(sectionElement => {
    sectionElement.innerText = messageInput.value;
  })
})

const audrey = document.getElementById("audrey")

/*
    Add an event listener to the `document` object to listen
    for the "scroll" event.
*/
document.addEventListener("scroll", function () {
  /*
      Adjust the width of audrey to be 1/3 the value of
      `window.scrollY`. No lower than 50px, though.
  */
  // Adjust the height of audrey to be 1/4 the value of`window.scrollY`. No lower than 100px, though.
  audrey.style.minHeight = "100px";
  audrey.style.height = window.scrollY * .25 + "px";
  //    audrey.style.height = (window.scrollY * (1/4) +  `100px`);

  // Adjust the width of audrey to be 1/3 the value of `window.scrollY`. No lower than 50px, though.
  audrey.style.minWidth = "50px";
  audrey.style.width = window.scrollY * .3 + "px";

  //console logging the values 
  console.log(window.scrollY, audrey.style.height)

})

// Add the correct string as the first argument to addEventListener()
// Write a function named flightHandlerFunction that will remove the disabled class on the corresponding < section id = "flight" > (section, not button) and replace it with a class of enabled.
// Have your developer tools open.When you click the button, the following element...
//   < section id = "flight" class="power disabled" >
//     Should then look like this.

// < section id = "flight" class="power enabled" >

const enableOnePower = (powerParam) => {
  const power = event.target.split("-")
  document.querySelector(`#activate-${powerParam}`).addEventListener("click", () => {
    const targetElement = document.querySelector(`#${powerParam}`);
    targetElement.classList.remove("disabled");
    targetElement.classList.add("enabled");
  })
}


const powerSections = document.querySelectorAll(".power");

// event listener for activte all
// document.querySelector("#activate-all").addEventListener("click", () => {
//   for (const singleSection of powerSections) {
//     singleSection.classList.add("enabled")
//     singleSection.classList.remove("disabled");
//   }
// })

// // event listener for deactivate all
// document.querySelector("#deactivate-all").addEventListener("click", () => {
//   for (const singleSection of powerSections) {
//     singleSection.classList.add("disabled")
//     singleSection.classList.remove("enabled");
//   }
// })


// enableOnePower("flight");
// enableOnePower("mindreading");
// enableOnePower("xray");



// Once that is complete, add two more buttons

// Enable All Powers
// Disable All Powers
// Write two more event handlers for activating and deactivating all powers when the corresponding buttons are clicked.You will need to use the document.querySelectorAll() method for these.

// Challenge: One Function to Rule Them All
// The learning objective of this challenge to write a function handler to be used for multiple events, and uses information in the event argument to perform common logic.

// You may notice that your code to enable individual powers(not all at once) is very similar.To keep your code DRY, make one function that will handle activating a power depending on which button is clicked. (Hint: one way to get started is to use event.target.id.split("-") in your function)

// const dealWithAllPowers = (actionParam) => {
//   // TERNARY OPERATORS!! WOOO!!
//   const classToAdd = actionParam === "activate" ? "enabled" : "disabled"
//   const classToRemove = actionParam === "activate" ? "disabled" : "enabled";
//   for (const singleSection of powerSections) {
//     singleSection.classList.add(classToAdd)
//     singleSection.classList.remove(classToRemove);
//   }
// }

// const dealWithOnePower = (powerParam) => {
//   const targetElement = document.querySelector(`#${powerParam}`);
//   targetElement.classList.remove("disabled");
//   targetElement.classList.add("enabled");
// }

// const handleAllButtons = () => {
//   const allButtons = document.querySelectorAll("button");
//   for (const singleButton of allButtons) {
//     singleButton.addEventListener("click", () => {
//       const power = event.target.id.split("-")[1];
//       if (power === "all") {
//         const action = event.target.id.split("-")[0];
//         dealWithAllPowers(action);
//       } else {
//         dealWithOnePower(power);
//       }
//     })
//   }
// }

// handleAllButtons();




// Create an HTML page that contains a text area and a button labeled Create.
// When the user enters in text into the text area and then clicks the create button, use a factory function that creates a new DOM component that has a border, and includes it's own delete button.
// Insert that new component into the DOM.
// When the user clicks the Delete button, the containing card, and no other cards, should then be removed from the DOM.Not just made invisible, actually removed from the DOM.
// Pro tip: The card's id attribute, and the button's id attribute should share some common value.Then, when the button is clicked, find the corresponding parent DOM component.Remember the split() method on a string ? That will be helpful.


{/* 
  <article class="card" id="card--2">
    <div>Hey, I entered some text</div>
    <div>
      <button id="delete--2">Delete This Card</button>
    </div>
  </article> */}

let counter = 0;
const cardContainer = document.querySelector("#card-container");
const createCard = () => {
  const text = document.querySelector("#text-input").value
  const cardComponent = `
  <article class="card" id="card--${counter}">
  <div>${text}</div>
  <div>
    <button id="delete--${counter}">Delete This Card</button>
  </div>
</article>`
  counter++;
  return cardComponent;
}

document.querySelector("#create-btn").addEventListener("click", () => {
  cardContainer.innerHTML += createCard();

})

document.querySelector("#card-container").addEventListener("click", () => {
  const currentId = event.target.id;
  if (currentId.split("--")[0] === "delete") {
    document.querySelector(`#card--${currentId.split("--")[1]}`).remove();
  }
})