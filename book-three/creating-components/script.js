// Practice: Student Components
// Create a new project.
// Paste the array of student objects above into your JavaScript file.

const students = [
  {
    name: "Chris Miller",
    class: "History",
    info: "Failed last exam",
    score: 59
  },
  {
    name: "Courtney Seward",
    class: "History",
    info: "Has completed all homework",
    score: 91
  },
  {
    name: "Garrett Ward",
    class: "History",
    info: "Wonderful at helping other students",
    score: 88
  },
  {
    name: "John Dulaney",
    class: "History",
    info: "Has never missed a class or exam",
    score: 92
  },
  {
    name: "Greg Lawrence",
    class: "History",
    info: "Sub-par performance all around",
    score: 64
  },
  {
    name: "Leah Duvic",
    class: "History",
    info: "Wonderful student",
    score: 97
  },
  {
    name: "Jesse Page",
    class: "History",
    info: "Smokes too much. Distracting.",
    score: 76
  },
  {
    name: "Kevin Haggerty",
    class: "History",
    info: "Falls asleep in class",
    score: 79
  },
  {
    name: "Max Wolf",
    class: "History",
    info: "Talks too much",
    score: 83
  },
  {
    name: "Lissa Goforth",
    class: "History",
    info: "Asks pointless, unrelated questions",
    score: 78
  },
  {
    name: "Tyler Bowman",
    class: "History",
    info: "When was the last time he attended class?",
    score: 48
  },
  {
    name: "Ray Medrano",
    class: "History",
    info: "Needs to contribute to in-class discussions",
    score: 95
  }
]
// Put the h1, section, and aside functions into your JavaScript file.

const h1 = (title, style) => {
  return `<h1 class="${style}">${title}</h1>`
}

const section = (title, style) => {
  return `<section class="bordered dashed ${style}">${title}</section>`
}

const aside = (title, style) => {
  return `<aside class="${style}">${title}</aside>`
}



// Iterate the array of students, and apply the correct style to the h1 depending on the score of the student being below 60, or above it.
let htmlString = "";
for (student of students) {
  let studentComponent = "";
  if (student.score >= 60) {
    studentComponent = `${h1(student.name, "xx-large passing")}`
  } else {
    studentComponent = `${h1(student.name, "xx-large failing")}`
  }
  studentComponent += `${section(student.class, "bordered dashed section--padded")} ${aside(student.info, "pushRight")}`
  htmlString += studentComponent;
}

document.querySelector("#container").innerHTML = htmlString;



// TO DO: pr to fix example code to xx-large passing in stead of xx-large green
// If a student is passing, then the structure should look like the following.

// <div class="student">
//     <h1 class="xx-large green">Student Name</h1>
//     <section class="bordered dashed section--padded">Student class</section>
//     <aside class="pushRight">Additional information</aside>
// </div>


// Challenges are optional exercises that you should only attempt if you have completed the practice exercises, and fully understand the concepts used in them.

// Challenge: Use Rest Operator
// This will allow you to pass as many arguments to your component-building functions as you want without the need to define each one in the argument list.

// const h1 = (...props) => {
//     return `< h1 class="${props[0]}" > ${ props[1] }</h1 > `
// }
// Change your functions to use the rest operator.


// const section = (...props) => {
//   return `<section class="bordered dashed ${props[0]}">${props[1]}</section>`
// }

// const aside = (...props) => {
//   return `<aside class="${props[0]}">${props[1]}</aside>`
// }


// Challenge: Generic HTML Function
// Look at the three functions you created to build an h1, a section, and an aside. Notice that each one follows the same pattern of applying the style argument, and the content argument in the same locations.

// expected params: element type, classname, content
const genericHTML = (...props) => {
  return `<${props[0]} class="${props[1]}">${props[2]}</${props[0]}>`
}

document.querySelector("#generic-test").innerHTML = genericHTML("h1", "xx-large passing", "does this work?")

// Advanced Challenge: Using createElement for Components
// The learning objective of this challenge is to move away from using string templates completely, and use the methods of createElement() and appendChild() to create DOM components.

// createElement()
// JavaScript provides the document.createElement() method which creates a virtual DOM element that doesn't exist in the DOM until you add it.

// appendChild()
// The document.appendChild() method will take a virtual DOM element you created and attach it as a child element of another one.

// Usage
// // Create an unordered list element
// const list = document.createElement('ul')

// // Create a list item for our list
// const listItem = document.createElement('li')
// listItem.className = "president"
// listItem.textContent = "George Bush"

// // Put the list item on the unordered list
// list.appendChild(listItem)

// console.log(list)
// When you insert the list object to the DOM, it will generate the following HTML.

// <ul>
//     <li class="president">George Bush</li>
// </ul>
// Creating Elements
// Using createElement(), you're going to create a simple list of chat messages that you might have with one of your family members. Maybe it's that wacky aunt that you see every Christmas and Fourth of July.

// Put an article DOM element in your index.html with the id attribute value of messages.
// In your JavaScript, use querySelector() to obtain a reference to that article DOM element.
// Create five (5) section components, each with a class of message, and with the content of your choosing.
// Using appendChild(), attach each message as a child to the messages element.
// Example output.

// <article id="messages">
//     <section class="message">
//         Are we doing fireworks this year?
//     </section>
//     <section class="message">
//         After last year's "tree incident", should we?
//     </section>
//     <section class="message">
//         The fire fighters put it out in like a minute. Wasn't even a real fire.
//     </section>
//     <section class="message">
//         We can set them off in the street.
//     </section>
//     <section class="message">
//         Our neighbors' houses are flammable, too
//     </section>
// </article>
// Advanced Challenge: DOM Fragments
// createDocumentFragment()
// What you did in the previous challenge was actually an expensive way of adding those elements to the DOM. Now, it didn't cost you any extra money, but it was expensive in terms of memory and processing power. When the DOM is written to, layout is 'invalidated', and at some point needs to be reflowed.

// We can use a document fragment to reduce the processing and layout cost of constant, rapid-fire DOM updates. You can add as many components to it as you like, and then add the fragment to the DOM as a one-time operation.

// // A new "virtual" document fragment to contain components
// const fragment = document.createDocumentFragment()

// // Create an emperor component element
// const julius = document.createElement('div')
// julius.textContent = "Julius Caesar"
// fragment.appendChild(julius)

// // Create an emperor component element
// const augustus = document.createElement('div')
// augustus.textContent = "Augustus Caesar"
// fragment.appendChild(augustus)

// // Create an emperor component element
// const aurelius = document.createElement('div')
// aurelius.textContent = "Marcus Aurelius"
// fragment.appendChild(aurelius)

// /*
//     Now I can update my HTML document all at once, with all
//     three emperor components rendered. Otherwise, I would
//     have needed to add each one, individually - a much more
//     "expensive" operation.
// */
// document.querySelector("#emperorList").appendChild(fragment)
// Practice Document Fragment
// Update your code from the previous challenge to add the chat messages to the messages elements via a document fragment.