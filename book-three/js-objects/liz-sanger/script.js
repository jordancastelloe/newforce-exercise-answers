// Her congressional district(you can use yours here)
// Her platform statements for the following issues.
//   Taxes
// Jobs
// Infrastructure
// Health care
// Crime and enforcement
// URL for donation form
// Calendar of events
// Volunteer information
// Name
// Address
// Email
// Phone number
// Availability
// What activities each one is willing to do (e.g.answering phone calls, taking polls, etc.)
// Biography
// Image gallery
// Head shot
// Picture of family
// Picture of constituents
// Mission statement
// URL for registering to vote

// this can look however they want
const lizSangerObject = {
  district: "3rd Congressional District of WV",
  platformStatements: {
    taxes: "Less of them!",
    jobs: "More of them!",
    infrastructure: "Great!",
    healthCare: "Don't get sick",
    crime: "Don't steal stuff",
  },
  donationFormURL: "www.liz.com/donate",
  eventsCalendar: [
    {
      event: "Meet and greet one",
      date: "March 1, 2019"
    },
    {
      event: "Meet and greet two",
      date: "March 3, 2019"
    },
    {
      event: "Meet and greet three",
      date: "March 5, 2019"
    }
  ],
  volunteerInfo: [
    {
      name: "Jim Bob",
      address: "123 Sesame Street",
      email: "jim@jim.com",
      phone: "222-2222",
      availability: "Tuesdays and Thursdays",
      preferredActivities: "canvassing"
    },
    {
      name: "Lucy Lu",
      address: "124 Sesame Street",
      email: "lucy@lu.com",
      phone: "222-2223",
      availability: "Wednesdays and Fridays",
      preferredActivities: "answering phones"
    }
  ],
  biography: "Born in Kentucky, raised in North Carolina, mother of four, etc",
  imageGallery: [
    {
      caption: "Liz with family",
      url: "dkajfjghdkjahgdakjsgh.png"
    },
    {
      caption: "Liz with dogs",
      url: "lakdhgajhgkjsahgkjaghkjhga.jpg"
    },
    {
      caption: "Liz with constituents",
      url: "lkahdgjkashgkjawhriueroew.jpg"
    },
    {
      caption: "headshot",
      url: "headshot.jpg"
    }
  ],
  missionStatement: "To infinity and beyooooooooond",
  registerToVoteURL: "www.vote.com/register"
}

// After you have defined all the objects for representing the data about Elizabeth's campaign, write a corresponding function for each one whose purpose is to change the state of the object. Then use your functions to modify the existing data.

console.log("this is the liz sanger object before we do anything to it", lizSangerObject);

const changeDistrict = (newDistrict) => {
  lizSangerObject.district = newDistrict;
}

changeDistrict("rodeo town usa")

const changePlatformStatement = (statementToChange, newStatement) => {
  lizSangerObject.platformStatements[statementToChange] = newStatement;
}

changePlatformStatement("taxes", "More of them! hooray for taxes!");

const addEvent = (eventName, eventDate) => {
  const eventObjectToAdd = {
    event: eventName,
    date: eventDate
  };
  lizSangerObject.eventsCalendar.push(eventObjectToAdd);
}

addEvent("kissing babies", "today");


console.log("after we did stuff to the object", lizSangerObject);